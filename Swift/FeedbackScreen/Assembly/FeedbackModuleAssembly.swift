//
//  FeedbackModuleAssembly.swift
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 15/01/2018.
//  Copyright © 2018 bsarkisian.me. All rights reserved.
//

import Swinject

class FeedbackModuleAssembly {
    
    class func activate(with container: Container) {
        container.storyboardInitCompleted(FeedbackViewController.self) { r, c in
            c.mailService = r.resolve(MailServiceProtocol.self)
        }
    }
    
}
