//
//  FeedbackViewController.swift
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 15/01/2018.
//  Copyright © 2018 bsarkisian.me. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift
import Typist

private let SuccessfullMessage = "Done. Thanks!\n"
private let FailureMessage = "Something went wrong 🙁\nTry again later."

final class FeedbackViewController: UIViewController {
    
    // MARK: - External dependencies
    
    var mailService: MailServiceProtocol!
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var feedbackTextView: TextView!
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendButton.layer.setupComeBackShadow(
            offsetY: sendButton.frame.height,
            width: sendButton.frame.width
        )
        
        setupFeedbackTextView()
        addKeyboardObservers()
    }
    
    deinit {
        Typist.shared.stop()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - IBActions
    
    @IBAction private func backButtonWasPressed() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func sendButtonWasPressed() {
        messageLabel.alpha = 0.0
        
        showHUD()
        
        mailService.sendMail(
            name: nameTextField.text,
            email: emailTextField.text,
            text: feedbackTextView.text
        ) { [weak self] successfully in
            self?.hideHUD()

            if successfully {
                self?.nameTextField.text = nil
                self?.emailTextField.text = nil
                self?.feedbackTextView.text = nil
                self?.view.endEditing(true)
            }

            self?.messageLabel.text = successfully ? SuccessfullMessage : FailureMessage
            self?.messageLabel.alpha = Alpha.show

            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                UIView.animate(withDuration: Animation.duration) {
                    self?.messageLabel.alpha = Alpha.hide
                }
            })
        }
    }
    
    @IBAction private func textFieldContentHaveChanged() {
        setupSendButton()
    }
    
    @IBAction private func contentViewWasTapped() {
        scrollView.endEditing(true)
    }
    
}

// MARK: - UITextFieldDelegate

extension FeedbackViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            feedbackTextView.becomeFirstResponder()
        default:
            break
        }
        
        return true
    }
    
}

// MARK: - UITextViewDelegate

extension FeedbackViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        setupSendButton()
    }
    
}

// MARK: - Private functions

extension FeedbackViewController {
 
    private func setupFeedbackTextView() {
        feedbackTextView.placeholderFont = UIFont(name: "Geometria", size: 15.0)
        feedbackTextView.textContainerInset = UIEdgeInsetsMake(12.0, 13.0, 0.0, 0.0)
    }
    
    private func setupSendButton() {
        let nameTextFieldIsValid = nameTextField.text?.count ?? 0 > 0
        let emailTextFieldIsValid = emailTextField.text?.count ?? 0 > 0
        let feedbackTextViewIsValid = feedbackTextView.text?.count ?? 0 > 0
        
        sendButton.isEnabled = nameTextFieldIsValid || emailTextFieldIsValid || feedbackTextViewIsValid
        sendButton.alpha = sendButton.isEnabled ? 1.0 : 0.7
        messageLabel.alpha = Alpha.hide
    }
    
    private func addKeyboardObservers() {
        Typist.shared
            .on(event: .willShow) { [weak self] (options) in
                let contentSizeHeight = self?.scrollView.contentSize.height ?? 0
                let boundsSizeHeight = self?.scrollView.bounds.size.height ?? 0
                
                let bottomOffset = CGPoint(
                    x: 0,
                    y: contentSizeHeight - boundsSizeHeight + options.endFrame.height
                )
                
                UIView.animate(withDuration: options.animationDuration) {
                    self?.scrollView.contentInset.bottom = options.endFrame.height
                    self?.scrollView.scrollIndicatorInsets.bottom = options.endFrame.height
                    self?.scrollView.contentOffset = bottomOffset
                }
            }
            .on(event: .willHide) { [weak self] (options) in
                UIView.animate(withDuration: options.animationDuration) {
                    self?.scrollView.contentInset.bottom = 0
                    self?.scrollView.scrollIndicatorInsets.bottom = 0
                }
            }
            .start()
    }
    
}
