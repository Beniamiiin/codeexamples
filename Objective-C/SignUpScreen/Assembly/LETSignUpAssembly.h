//
//  LETSignUpAssembly.h
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import <Typhoon/Typhoon.h>

#import <RamblerTyphoonUtils/AssemblyCollector.h>

@class LETServiceAssembly;

@interface LETSignUpAssembly : TyphoonAssembly <RamblerInitialAssembly>

@property (nonatomic, readonly, strong) LETServiceAssembly *serviceAssembly;

@end