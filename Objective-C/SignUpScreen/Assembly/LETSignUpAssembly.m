//
//  LETSignUpAssembly.m
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETSignUpAssembly.h"

#import "LETSignUpViewController.h"
#import "LETSignUpInteractor.h"
#import "LETSignUpPresenter.h"
#import "LETSignUpRouter.h"

#import "LETServiceAssembly.h"

@implementation LETSignUpAssembly

- (LETSignUpViewController *)viewSignUpModule 
{
    return [TyphoonDefinition withClass:[LETSignUpViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSignUpModule]];
                          }];
}

- (LETSignUpInteractor *)interactorSignUpModule 
{
    return [TyphoonDefinition withClass:[LETSignUpInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSignUpModule]];
                              [definition injectProperty:@selector(authService)
                                                    with:[self.serviceAssembly authService]];
                              [definition injectProperty:@selector(userInfoStorageService)
                                                    with:[self.serviceAssembly userInfoStorageService]];
                              [definition injectProperty:@selector(credentialsStorageService)
                                                    with:[self.serviceAssembly credentialsStorageService]];
                              [definition injectProperty:@selector(validationService)
                                                    with:[self.serviceAssembly validationService]];
                          }];
}

- (LETSignUpPresenter *)presenterSignUpModule 
{
    return [TyphoonDefinition withClass:[LETSignUpPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSignUpModule]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSignUpModule]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSignUpModule]];
                          }];
}

- (LETSignUpRouter *)routerSignUpModule 
{
    return [TyphoonDefinition withClass:[LETSignUpRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSignUpModule]];
                          }];
}

@end