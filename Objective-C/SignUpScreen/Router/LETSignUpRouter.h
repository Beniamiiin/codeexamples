//
//  LETSignUpRouter.h
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETSignUpRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface LETSignUpRouter : NSObject <LETSignUpRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end