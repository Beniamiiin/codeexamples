//
//  LETSignUpRouter.m
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETSignUpRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

#import "LETStoryboardConstants.h"

#import "LETEditUserModuleInput.h"

@implementation LETSignUpRouter

#pragma mark - LETSignUpRouterInput
- (void)showEditUserInfoModule
{
	[[self.transitionHandler openModuleUsingSegue:kLETEditUserInfoModuleSegue] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<LETEditUserModuleInput> moduleInput) {
        [moduleInput afterRegistration:YES];
        return nil;
    }];
}

@end