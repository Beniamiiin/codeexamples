//
//  LETSignUpRouterInput.h
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LETSignUpRouterInput <NSObject>

- (void)showEditUserInfoModule;

@end