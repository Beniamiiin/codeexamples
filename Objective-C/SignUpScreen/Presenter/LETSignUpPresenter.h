//
//  LETSignUpPresenter.h
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETSignUpViewOutput.h"
#import "LETSignUpInteractorOutput.h"

@protocol LETSignUpViewInput;
@protocol LETSignUpInteractorInput;
@protocol LETSignUpRouterInput;

@interface LETSignUpPresenter : NSObject <LETSignUpViewOutput, LETSignUpInteractorOutput>

@property (nonatomic, weak) id<LETSignUpViewInput> view;
@property (nonatomic, strong) id<LETSignUpInteractorInput> interactor;
@property (nonatomic, strong) id<LETSignUpRouterInput> router;

@end