//
//  LETSignUpPresenter.m
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETSignUpPresenter.h"

#import "LETSignUpViewInput.h"
#import "LETSignUpInteractorInput.h"
#import "LETSignUpRouterInput.h"

#import "LETConstants.h"
#import "LETAuthInfo.h"

@implementation LETSignUpPresenter

#pragma mark - LETSignUpViewOutput
- (void)didTriggerViewReadyEvent 
{
    [self.view setupSignUpButtonWithEnabledState:NO];
    [self.view setupPhoneTextFieldWithPattern:kLETPhoneNumberPattern];
}

- (void)didTriggerViewWillAppear
{
    [self.view subscribeKeyboad];
}

- (void)didTriggerViewWillDisappear
{
    [self.view unsubscribeKeyboard];
}

- (void)didTriggerKeyboardShowingWithContentRect:(CGRect)contentRect
                             navigationBarHeight:(CGFloat)navigationBarHeight
                                    keyboardRect:(CGRect)keyboardRect
{
    contentRect.origin.y += navigationBarHeight;
    
    CGFloat bottomMargin = 0.0;
    
    if ( CGRectIntersectsRect(keyboardRect, contentRect) )
    {
        bottomMargin = CGRectGetHeight(CGRectIntersection(keyboardRect, contentRect));
    }
    
    contentRect.origin.y -= navigationBarHeight;
    
    double intersects = CGRectGetMinY(contentRect) - bottomMargin;
    CGFloat contentTopOffset = intersects > 0 ? intersects : 0.0;
    CGFloat contentBottomOffset = intersects < 0 ? fabs(intersects) : 0.0;
    
    [self.view setupScrollViewWithTopOffset:contentTopOffset bottomOffset:contentBottomOffset];
}

- (void)didTriggerKeyboardHidding
{
    [self.view setupScrollViewWithOriginalTopAndBottomOffsets];
}

- (void)didTriggerPhoneTextFieldShouldBeginEditing:(NSString *)textFieldText
{
    if ( textFieldText.length )
    {
        return;
    }
    
    [self.view setupPhoneTextFieldWithPrefix:kLETPhoneNumberPrefix];
}

- (void)didTriggerPhoneTextFieldShouldEndEditing:(NSString *)textFieldText
{
    if ( ![textFieldText isEqualToString:kLETPhoneNumberPrefix] )
    {
        return;
    }
    
	[self.view setupPhoneTextFieldWithPrefix:nil];
}

- (void)didTriggerTextFieldsChange
{
	BOOL fieldsIsValid = [self.interactor isValidTextFieldsWithEmail:[self.view email]
                                                               phone:[self.view phone]
                                                            password:[self.view password]
                                                      repeatPassword:[self.view repeatPassword]];
    
    [self.view setupSignUpButtonWithEnabledState:fieldsIsValid];
}

- (void)didTriggerSignUpButtonPressed
{
    [self.view hideKeyboard];

    LETAuthInfo *authInfo = [LETAuthInfo new];
    authInfo.email = [self.view email];
    authInfo.phone = [self.view phone];
    authInfo.password = [self.view password];
    
    [self.interactor signUpWithAuthInfo:authInfo];
}

#pragma mark - LETSignUpInteractorOutput
- (void)signUpWillStart
{
    [self.view showHUD];
}

- (void)signUpDidFinish
{
    [self.view hideHUD];
}

- (void)signUpDidFinishWithError:(NSError *)error
{
    if ( error )
    {
        [self.view showErrorMessageInAlertView:error.localizedDescription];
        return;
    }
    
    [self.router showEditUserInfoModule];
}

@end