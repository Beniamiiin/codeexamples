//
//  LETSignUpViewOutput.h
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol LETSignUpViewOutput <NSObject>

- (void)didTriggerViewReadyEvent;
- (void)didTriggerViewWillAppear;
- (void)didTriggerViewWillDisappear;

- (void)didTriggerKeyboardShowingWithContentRect:(CGRect)contentRect
                             navigationBarHeight:(CGFloat)navigationBarHeight
                                    keyboardRect:(CGRect)keyboardRect;
- (void)didTriggerKeyboardHidding;

- (void)didTriggerPhoneTextFieldShouldBeginEditing:(NSString *)textFieldText;
- (void)didTriggerPhoneTextFieldShouldEndEditing:(NSString *)textFieldText;
- (void)didTriggerTextFieldsChange;
- (void)didTriggerSignUpButtonPressed;

@end