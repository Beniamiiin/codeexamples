//
//  LETSignUpViewController.m
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETSignUpViewController.h"

#import "LETSignUpViewOutput.h"

#import <SHSPhoneComponent/SHSPhoneTextField.h>
#import <UIViewController+KeyboardAnimation/UIViewController+KeyboardAnimation.h>

@interface LETSignUpViewController ()

@property (nonatomic, assign) CGFloat originalTopConstant;
@property (nonatomic, assign) UIEdgeInsets originalContentInset;

@end

@implementation LETSignUpViewController

#pragma mark - Методы жизненного цикла
- (void)viewDidLoad 
{
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.output didTriggerViewWillAppear];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.output didTriggerViewWillDisappear];
}

#pragma mark - LETSignUpViewInput
- (void)subscribeKeyboad
{
    self.originalTopConstant = self.contentScrollViewTopToSuperViewConstaint.constant;
    self.originalContentInset = self.contentScrollView.contentInset;
    
    [self an_subscribeKeyboardWithAnimations:^(CGRect keyboardRect, NSTimeInterval duration, BOOL isShowing) {
        if ( isShowing )
        {
            CGFloat statusBarHeight = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
            CGFloat navigationBarHeight = CGRectGetHeight(self.navigationController.navigationBar.bounds);
            CGFloat fullNavigationBarHeight = statusBarHeight + navigationBarHeight;
            
            [self.output didTriggerKeyboardShowingWithContentRect:self.contentScrollView.frame
                                              navigationBarHeight:fullNavigationBarHeight
                                                     keyboardRect:keyboardRect];
        }
        else
        {
            [self.output didTriggerKeyboardHidding];
        }
        
        [self.contentScrollView.superview layoutSubviews];
    } completion:nil];
}

- (void)unsubscribeKeyboard
{
    [self an_unsubscribeKeyboard];
}

- (void)setupScrollViewWithTopOffset:(CGFloat)topOffset bottomOffset:(CGFloat)bottomOffset
{
    self.contentScrollViewTopToSuperViewConstaint.constant = topOffset;
    
    UIEdgeInsets contentInset = self.contentScrollView.contentInset;
    contentInset.bottom = bottomOffset;
    self.contentScrollView.contentInset = contentInset;
    self.contentScrollView.scrollIndicatorInsets = contentInset;
}

- (void)setupScrollViewWithOriginalTopAndBottomOffsets
{
    [self setupScrollViewWithTopOffset:self.originalTopConstant bottomOffset:self.originalContentInset.bottom];
}

- (void)setupPhoneTextFieldWithPattern:(NSString *)pattern
{
    [self.phoneTextField.formatter setDefaultOutputPattern:pattern];
}

- (void)setupPhoneTextFieldWithPrefix:(NSString *)prefix
{
    self.phoneTextField.formatter.prefix = prefix;
}

- (void)setupSignUpButtonWithEnabledState:(BOOL)enabled
{
    self.signUpButton.enabled = enabled;
}

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (NSString *)email
{
    return self.emailTextField.text;
}

- (NSString *)phone
{
	return self.phoneTextField.text;
}

- (NSString *)password
{
	return self.passwordTextField.text;
}

- (NSString *)repeatPassword
{
	return self.repeatPasswordTextField.text;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if ( textField.returnKeyType != UIReturnKeyNext )
    {
        return [textField resignFirstResponder];
    }
    
    if ( textField == self.emailTextField )
    {
        [self.phoneTextField becomeFirstResponder];
    }
    else if ( textField == self.phoneTextField )
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if ( textField == self.passwordTextField )
    {
        [self.repeatPasswordTextField becomeFirstResponder];
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ( textField == self.phoneTextField )
    {
        [self.output didTriggerPhoneTextFieldShouldBeginEditing:textField.text];
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if ( textField == self.phoneTextField )
    {
        [self.output didTriggerPhoneTextFieldShouldEndEditing:textField.text];
    }
    
    return YES;
}

#pragma mark - IBActions
- (IBAction)textFieldValueDidChange
{
    [self.output didTriggerTextFieldsChange];
}

- (IBAction)signUpButtonAction
{
	[self.output didTriggerSignUpButtonPressed];
}

- (IBAction)backFromUserTermsModule:(UIStoryboardSegue *)segue {}

@end