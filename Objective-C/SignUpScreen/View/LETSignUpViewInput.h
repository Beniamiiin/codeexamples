//
//  LETSignUpViewInput.h
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETBaseViewInput.h"

@protocol LETSignUpViewInput <LETBaseViewInput>

- (void)subscribeKeyboad;
- (void)unsubscribeKeyboard;

- (void)setupScrollViewWithTopOffset:(CGFloat)topOffset bottomOffset:(CGFloat)bottomOffset;
- (void)setupScrollViewWithOriginalTopAndBottomOffsets;

- (void)setupPhoneTextFieldWithPattern:(NSString *)pattern;
- (void)setupPhoneTextFieldWithPrefix:(NSString *)prefix;

- (void)setupSignUpButtonWithEnabledState:(BOOL)enabled;
- (void)hideKeyboard;

- (NSString *)email;
- (NSString *)phone;
- (NSString *)password;
- (NSString *)repeatPassword;

@end