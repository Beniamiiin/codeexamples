//
//  LETSignUpViewController.h
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETBaseViewController.h"

#import "LETSignUpViewInput.h"

@class SHSPhoneTextField;
@protocol LETSignUpViewOutput;

@interface LETSignUpViewController : LETBaseViewController <LETSignUpViewInput, UITextFieldDelegate>

@property (nonatomic, strong) id<LETSignUpViewOutput> output;

@property (nonatomic, weak) IBOutlet UIScrollView *contentScrollView;
@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet SHSPhoneTextField *phoneTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;
@property (nonatomic, weak) IBOutlet UITextField *repeatPasswordTextField;
@property (nonatomic, weak) IBOutlet UIButton *signUpButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contentScrollViewTopToSuperViewConstaint;

- (IBAction)textFieldValueDidChange;
- (IBAction)signUpButtonAction;

@end