//
//  LETSignUpInteractorInput.h
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LETAuthInfo;

@protocol LETSignUpInteractorInput <NSObject>

- (BOOL)isValidTextFieldsWithEmail:(NSString *)email
                             phone:(NSString *)phone
                          password:(NSString *)password
                    repeatPassword:(NSString *)repeatPassword;
- (void)signUpWithAuthInfo:(LETAuthInfo *)authInfo;

@end