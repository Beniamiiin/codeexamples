//
//  LETSignUpInteractor.h
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETSignUpInteractorInput.h"

@protocol LETSignUpInteractorOutput;
@protocol LETAuthServiceProtocol;
@protocol LETUserInfoStorageServiceProtocol;
@protocol LETCredentialsStorageServiceProtocol;
@protocol LETValidationServiceProtocol;

@interface LETSignUpInteractor : NSObject <LETSignUpInteractorInput>

@property (nonatomic, weak) id<LETSignUpInteractorOutput> output;

@property (nonatomic, strong) id<LETAuthServiceProtocol> authService;
@property (nonatomic, strong) id<LETUserInfoStorageServiceProtocol> userInfoStorageService;
@property (nonatomic, strong) id<LETCredentialsStorageServiceProtocol> credentialsStorageService;
@property (nonatomic, strong) id<LETValidationServiceProtocol> validationService;

@end