//
//  LETSignUpInteractor.m
//  bsarkisian.me
//
//  Created by Beniamin Sarkisyan on 18/03/2016.
//  Copyright 2016 bsarkisian.me. All rights reserved.
//

#import "LETSignUpInteractor.h"

#import "LETSignUpInteractorOutput.h"
#import "LETAuthServiceProtocol.h"
#import "LETUserInfoStorageServiceProtocol.h"
#import "LETCredentialsStorageServiceProtocol.h"
#import "LETValidationServiceProtocol.h"

#import "LETProfileUser.h"
#import "LETAuthInfo.h"

@implementation LETSignUpInteractor

#pragma mark - LETSignUpInteractorInput
- (BOOL)isValidTextFieldsWithEmail:(NSString *)email
                             phone:(NSString *)phone
                          password:(NSString *)password
                    repeatPassword:(NSString *)repeatPassword
{
    BOOL emailIsValid = [self.validationService emailIsValid:email];
    BOOL phoneIsValid = [self.validationService phoneIsValid:phone];
    BOOL passwordIsValid = [self passwordIsValid:password repeatPassword:repeatPassword];
    
    return emailIsValid && phoneIsValid && passwordIsValid;
}

- (void)signUpWithAuthInfo:(LETAuthInfo *)authInfo
{
    [self.output signUpWillStart];
    
    [self.authService signUpWithAuthInfo:authInfo completionBlock:^(LETProfileUser *user, NSError *error) {
        [self.output signUpDidFinish];
        
        if ( user )
        {
            [self.userInfoStorageService saveUser:user];
            [self.credentialsStorageService saveLogin:authInfo.email];
            [self.credentialsStorageService savePassword:authInfo.password];
        }
        
        [self.output signUpDidFinishWithError:error];
    }];
}

- (BOOL)passwordIsValid:(NSString *)password repeatPassword:(NSString *)repeatPassword
{
    return [self.validationService passwordIsValid:password] && [password isEqual:repeatPassword];
}

@end